use std::{
    io::{self, Stdout},
    sync::{
        mpsc::{channel, Receiver},
        Arc, Mutex,
    },
};

use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use tui::{
    backend::CrosstermBackend,
    style::{Color, Style},
    widgets::{Block, Borders, List, ListItem, ListState},
    Terminal,
};

fn create_terminal() -> io::Result<Terminal<CrosstermBackend<Stdout>>> {
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    Terminal::new(backend)
}

fn restore_terminal(terminal: &mut Terminal<CrosstermBackend<Stdout>>) -> io::Result<()> {
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    Ok(())
}

pub struct Tui {
    items: Vec<String>,
    terminal: Terminal<CrosstermBackend<Stdout>>,
    state: Arc<Mutex<ListState>>,
}

#[derive(Debug)]
pub enum TuiAction {
    Quit,
    ItemPressed(usize),
    Tick,
}

fn scroll_up(amt: usize, state: &mut ListState) {
    let cur = state.selected().unwrap_or(1);
    let next = if cur == 0 { amt - 1 } else { cur - 1 };
    state.select(Some(next));
}

fn scroll_down(amt: usize, state: &mut ListState) {
    let cur = state.selected().unwrap_or(amt - 1);
    let next = if cur >= amt - 1 { 0 } else { cur + 1 };
    state.select(Some(next));
}

impl Tui {
    /**
    Creates a new Tui object.

    ## Example

    ```no_run
    use checklist_tui::Tui;
    use std::error::Error;

    fn main() -> Result<(), Box<dyn Error>> {
        let items = vec!(
            String::from("Item 1"),
            String::from("Item 2"),
            String::from("Item 3"),
        );
        let mut tui = Tui::new(items)?;

        Ok(())
    }
    ```
    */
    pub fn new(list: Vec<String>) -> io::Result<Self> {
        let mut state = ListState::default();
        state.select(Some(0));
        let terminal = create_terminal()?;
        Ok(Tui {
            items: list,
            terminal,
            state: Arc::new(Mutex::new(state)),
        })
    }

    pub fn update_list(&mut self, new_list: Vec<String>) {
        self.items = new_list;
    }

    pub fn render_list(&mut self) -> io::Result<()> {
        self.terminal.draw(|f| {
            let items: Vec<ListItem> = self
                .items
                .iter()
                .map(|s| ListItem::new(s.as_str()))
                .collect();
            let list = List::new(items)
                .block(Block::default().title("List").borders(Borders::ALL))
                .highlight_style(Style::default().fg(Color::Black).bg(Color::Green));
            f.render_stateful_widget(list, f.size(), &mut self.state.lock().unwrap());
        })?;

        Ok(())
    }

    pub fn cleanup(&mut self) -> io::Result<()> {
        restore_terminal(&mut self.terminal)
    }

    /**
    Starts the event loop. Returns a receiver which can receive different TUI actions

    ## Example: handling quit events

    ```no_run
    use std::error::Error;
    use checklist_tui::{Tui, TuiAction};

    fn main() -> Result<(), Box<dyn Error>> {
        let mut tui = Tui::new(vec!())?;

        let rx = tui.start_event_loop();

        loop {
            let action = rx.recv().unwrap();

            match action {
                TuiAction::Quit => {
                    tui.cleanup()?;
                    return Ok(());
                }
                _ => {}
            }
        }
    }
    ```
    */
    pub fn start_event_loop(&mut self) -> Receiver<TuiAction> {
        let (tx, rx) = channel();

        let state = self.state.clone();
        let item_count = self.items.len();
        std::thread::spawn(move || {
            loop {
                // poll for tick rate duration, if no event, sent tick event.
                let new_event = event::poll(std::time::Duration::from_millis(100)).unwrap();

                if !new_event {
                    tx.send(TuiAction::Tick).unwrap();
                    continue;
                }

                if let Event::Key(key) = event::read().unwrap() {
                    match key.code {
                        KeyCode::Char('q') => tx.send(TuiAction::Quit).unwrap(),
                        KeyCode::Up => scroll_up(item_count, &mut state.lock().unwrap()),
                        KeyCode::Down => scroll_down(item_count, &mut state.lock().unwrap()),
                        KeyCode::Char(' ') => {
                            let mut state = state.lock().unwrap();
                            let selected = state.selected().unwrap_or(0);
                            tx.send(TuiAction::ItemPressed(selected)).unwrap();
                            scroll_down(item_count, &mut state);
                        }
                        _ => {}
                    }
                }
            }
        });

        rx
    }
}
